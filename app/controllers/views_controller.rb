class ViewsController < ApplicationController
  before_action :set_view, only: [:show, :edit, :update, :destroy]

  @@byView = "Views"
  # GET /views
  # GET /views.json
  def index
    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'Views' and profile_id = ?", @cu)

        @per.each do |permisos|
          @uno = permisos.view_name
          @crearView = permisos.crear
          @editarView = permisos.editar
          @leerView = permisos.leer
          @eliminarView = permisos.eliminar

          if permisos.view_name == @@byView

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@crear == 8) || (@@editar == 4) || (@@leer == 2) || (@@eliminar == 1))

          @views = View.all.page(params[:page]).per(10)

          if params[:search].present?

            name = params[:search]
            name = name.gsub! /\t/, ''
            if name.nil?
              @na = params[:search].to_s
            else
              @na = name
            end

            if !View.where("name like '%"+@na+"%'").nil?
              @views = View.where("name like '%"+@na+"%'").page(params[:page]).per(10)
              @query = params[:search]
            else
              @error = t('all.error')
              @query = params[:search]
              @views = View.all.page(params[:page]).per(10)
            end
          end


        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to home_index_path, :alert => t('all.not_access')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # GET /views/1
  # GET /views/1.json
  def show
    redirect_to home_index_path, :alert => t('all.not_access')
  end

  # GET /views/new
  def new
    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'Views' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarView = permisos.editar

          if permisos.view_name == @@byView

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@crear == 8))
          @view = View.new
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # GET /views/1/edit
  def edit
    if current_user
      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'Views' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarView = permisos.editar

          if permisos.view_name == @@byView

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@editar == 4))

        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # POST /views
  # POST /views.json
  def create
    if current_user
      @view = View.new(view_params)

      respond_to do |format|
        @view.crear = params[:view][:crear]
        @view.editar = params[:view][:editar]
        @view.eliminar = params[:view][:eliminar]
        @view.leer = params[:view][:leer]
        if @view.save

          # Permisos del administrador sobre la vista
          @profile_admon = Profile.find_by_flag(0)

          @permisos_admon = Permission.new
          @permisos_admon.view_id = @view.id
          @permisos_admon.view_name = @view.name

          if @view.crear == 1 || @view.crear == "1"
            @permisos_admon.crear = 8
          end

          if @view.editar == 1 || @view.editar == "1"
            @permisos_admon.editar = 4
          end

          if @view.leer == 1 || @view.leer == "1"
            @permisos_admon.leer = 2
          end

          if @view.eliminar == 1 || @view.eliminar == "1"
            @permisos_admon.eliminar = 1
          end

          @permisos_admon.profile_id = @profile_admon.id
          @permisos_admon.save

          #Permisos para otros perfiles sin permisos dentro de la vista
          @otros = Profile.where("flag != 0")

          @otros.each do |per|

            @permisos_otro = Permission.new
            @permisos_otro.view_id = @view.id
            @permisos_otro.view_name = @view.name
            @permisos_otro.profile_id = per.id
            @permisos_otro.save

          end

          format.html {redirect_to views_path, notice: t('views.mess_cont_view_sus')}
          format.json {render :show, status: :created, location: @view}
        else
          format.html {render :new}
          format.json {render json: @view.errors, status: :unprocessable_entity}
        end
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end
  end

  # PATCH/PUT /views/1
  # PATCH/PUT /views/1.json
  def update
    if current_user
      respond_to do |format|
        if @view.update(view_params)

          @view.crear = params[:view][:crear]
          @view.editar = params[:view][:editar]
          @view.leer = params[:view][:leer]
          @view.eliminar = params[:view][:eliminar]
          @view.save

          # Permisos del administrador sobre la vista
          @profile_admon = Profile.find_by_flag(0)

          @permisos_admon = Permission.find_by_view_id(@view.id)
          @permisos_admon.view_id = @view.id
          @permisos_admon.view_name = @view.name

          if @view.crear == 1 || @view.crear == "1"
            @permisos_admon.crear = 8
          else
            @permisos_admon.crear = nil
          end

          if @view.editar == 1 || @view.editar == "1"
            @permisos_admon.editar = 4
          else
            @permisos_admon.editar = nil
          end

          if @view.leer == 1 || @view.leer == "1"
            @permisos_admon.leer = 2
          else
            @permisos_admon.leer = nil
          end

          if @view.eliminar == 1 || @view.eliminar == "1"
            @permisos_admon.eliminar = 1
          else
            @permisos_admon.eliminar = nil
          end

          @permisos_admon.profile_id = @profile_admon.id
          @permisos_admon.save

          #Permisos para otros perfiles sin permisos dentro de la vista
          @otros = Profile.where("flag != 0")

          @otros.each do |per|

            @view_id = @view.id
            @profile_id = per.id

            @permisos_otro = Permission.find_by(view_id: @view.id, profile_id: per.id)
            @permisos_otro.view_id = @view.id
            @permisos_otro.view_name = @view.name
            @permisos_otro.profile_id = per.id
            @permisos_otro.save

          end

          format.html {redirect_to views_path, notice: t('views.mess_cont_view_up')}
          format.json {render :show, status: :ok, location: @view}
        else
          format.html {render :edit}
          format.json {render json: @view.errors, status: :unprocessable_entity}
        end
      end
    end
  end

  # DELETE /views/1
  # DELETE /views/1.json
  def destroy
    if current_user

      @cu = current_user.profile_id

      if @cu == 1

        @per = Permission.where("view_name = 'Views' and profile_id = ?", @cu)

        @per.each do |permisos|
          @editarView = permisos.editar

          if permisos.view_name == @@byView

            @@crear = permisos.crear
            @@editar = permisos.editar
            @@leer = permisos.leer
            @@eliminar = permisos.eliminar

            @crear = permisos.crear
            @editar = permisos.editar
            @leer = permisos.leer
            @eliminar = permisos.eliminar
          end

        end

        if ((@@eliminar == 1))

          @view.destroy

          @permisos_vista = Permission.where("view_id = ?", @view.id)

          @permisos_vista.each do |per|
            per.destroy
          end

          respond_to do |format|
            format.html {redirect_to views_url, notice: t('views.mess_cont_view_del')}
            format.json {head :no_content}
          end
        else
          @Without_Permission = 100
          redirect_to home_index_path, :alert => t('all.not_access')
        end
      else
        @Without_Permission = 100
        redirect_to new_user_session_path, :alert => t('all.please_continue')
      end
    else
      @Without_Permission = 100
      redirect_to new_user_session_path, :alert => t('all.please_continue')
    end

  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_view
    @view = View.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def view_params
    params.require(:view).permit(:name, :crear, :editar, :eliminar, :leer)
  end
end
